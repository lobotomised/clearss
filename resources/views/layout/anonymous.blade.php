<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="X-CSRF-TOKEN" value="{{ csrf_token() }}">
    <title>@yield('pageTitle') - Clearss</title>
    <link rel="stylesheet" href="{{ mix('build/css/core.css') }}" media="screen" type="text/css">
    <link rel="stylesheet" href="{{ mix('build/css/vendor.css') }}" media="screen" type="text/css">
    <link rel="stylesheet" href="{{ mix('build/css/app.css') }}" media="screen" type="text/css">
</head>
</html>
