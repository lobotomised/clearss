<?php

/*
|--------------------------------------------------------------------------
| Console Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of your Closure based console
| commands. Each Closure is bound to a command instance allowing a
| simple approach to interacting with each command's IO methods.
|
*/

Artisan::command('rss:feed:import', function (Clearss\Services\OplmService $opml) {
    //$opml->import('/home/vagrant/code/rss.rprevost.fr/storage/feedly.opml');
    $opml->import('/home/vagrant/code/rss.rprevost.fr/storage/file.opml.xml');
});

Artisan::command('rss:update', function (Clearss\Services\Updater $feed) {
    Log::info('Start: Full update');
    $feed->fetchAllFeed();
    Log::info('End: Full update');
});

Artisan::command('app:bootstrap', function () {
    Artisan::call('telescope:install');
    Artisan::call('migrate:refresh');
    Artisan::call('db:seed');
    Artisan::call('rss:feed:import');
    Artisan::call('rss:update');
});
