<?php

use Clearss\Models\Category;
use Faker\Generator as Faker;

$factory->define(Category::class, static function (Faker $faker) {
    return [
        'name' => $faker->sentence,
    ];
});
