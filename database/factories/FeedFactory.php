<?php

use Clearss\Models\Feed;
use Faker\Generator as Faker;

$factory->define(Feed::class, static function (Faker $faker) {
    return [
        'name'              => $faker->sentence,
        'url'               => $faker->url,
        'website'           => 'http://www'.$faker->domainName.'/',
        'last_batch'        => now(),
        'error'             => 0,
        'ttl'               => 1800,
        'nh_entries'        => 0,
        'nh_entries_unread' => 0,
    ];
});
