<?php

use Clearss\Models\Entry;
use Faker\Generator as Faker;

$factory->define(Entry::class, static function (Faker $faker) {
    return [
        'title'           => $faker->sentence,
        'content'         => $faker->sentence,
        'url'             => $faker->url,
        'read'            => 0,
        'favorite'        => 0,
        'last_updated_at' => now(),
    ];
});
