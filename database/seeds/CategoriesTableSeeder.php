<?php

use Clearss\Models\Category;
use Clearss\Models\User;
use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{

    public function run()
    {
        $user = factory(User::class)->create([
            'name'     => 'admin',
            'email'    => 'admin@example.test',
            'password' => 'admin',
        ]);

        Category::create(['name' => 'Sans Catégorie', 'user_id' => $user->id]);
    }
}
