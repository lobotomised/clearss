<?php

return [
    'day_retention' => env('RSS_DAY_RETENTION', 30),
    'cache_path'    => env('RSS_CACHE_PATH', 'rss/cache'),
    'favicon_path'  => env('RSS_FAVICON_PATH', 'rss/favicons'),
    'ttl'           => env('RSS_TTL', 1800), //30 min
];
