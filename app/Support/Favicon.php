<?php

namespace Clearss\Support;

use DOMDocument;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use SimplePie_IRI;
use function in_array;

class Favicon
{

    public function __construct()
    {
        $this->favicon_path = config('rss.favicon_path').'/';
    }

    /**
     * Return path of the favicon.ico.
     *
     * @param  \Illuminate\Database\Eloquent\Model|int  $feed
     *
     * @return string
     */
    public function getPath($feed): string
    {
        $id = $feed instanceof Model ? $feed->getKey() : $feed;

        return config('rss.favicon_path').'/'.$id.'.ico';
    }

    public function getFavicon($feed): string
    {
        if ( ! Storage::disk('local')->exists($this->getPath($feed))) {
            $this->downloadFavicon($feed);
        }

        $file = Storage::disk('local')->get($this->getPath($feed));

        return $file;
    }

    public function refresh(Model $feed): void
    {
        $exists = Storage::disk('local')->exists($this->getPath($feed));

        if ($exists) {
            $timestamp = filemtime(storage_path('app/'.$this->getPath($feed)));
            $time = Carbon::createFromTimestamp($timestamp);

            if (Carbon::now()->subDays(config('rss.day_retention'))->lt($time)) {
                return;
            }
        }

        $this->downloadFavicon($feed);
    }

    /**
     * Remove a favicon from the storage.
     *
     * @param  \Illuminate\Database\Eloquent\Model  $feed
     *
     * @return bool
     */
    public function deleteFavicon(Model $feed)
    {
        return Storage::disk('local')->delete($this->getPath($feed));
    }

    public function http_get($url): ?string
    {
        $ch = curl_init($url);
        curl_setopt_array($ch, [
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_TIMEOUT        => 10,
            CURLOPT_USERAGENT      => 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:64.0) Gecko/20100101 Firefox/64.0',
            CURLOPT_MAXREDIRS      => 3,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_ENCODING       => '',
        ]);

        $response = curl_exec($ch);
        $info = curl_getinfo($ch);
        curl_close($ch);

        if ($info['http_code'] === 200) {
            return $response;
        }

        return null;
    }

    private function downloadFavicon(Model $feed): void
    {
        Log::debug('Téléchargement de '.$feed->website.'/favicon.ico');
        $file = $this->http_get($feed->website.'/favicon.ico');

        if ($file) {
            $this->writeFavicon($feed, $file);
        } else {
            $file_url = $this->searchFavicon($feed);
            $file = $this->http_get($file_url);
            if ($file) {
                $this->writeFavicon($feed, $file);
            }
        }
    }

    private function writeFavicon($feed, $file): void
    {
        if (Storage::disk('local')->exists($this->getPath($feed))) {
            Storage::disk('local')->delete($this->getPath($feed));
        }

        Storage::disk('local')->put($this->getPath($feed), $file);
    }

    private function searchFavicon(Model $feed): ?string
    {
        $website = $this->http_get($feed->website);

        if ($website) {
            $dom = new DOMDocument();
            libxml_use_internal_errors(true);
            $dom->loadHTML($website);
            $links = $dom->getElementsByTagName('link');

            foreach ($links as $meta_link) {
                /** @var \DOMElement $meta_link */
                if ($meta_link->hasAttribute('rel') && $meta_link->hasAttribute('href')) {
                    $rel = mb_strtolower($meta_link->getAttribute('rel'));
                    if (in_array($rel, ['icon', 'shortcut icon'])) {
                        $href = mb_strtolower(trim($meta_link->getAttribute('href')));

                        return $this->makeRealUrl($href, $feed);
                    }
                }
            }
        }

        return null;
    }

    /**
     * Transforme une url relative, en url absolue.
     *
     * @param $url
     * @param $feed
     *
     * @return string
     */
    private function makeRealUrl($url, Model $feed): string
    {
        // si il ne manque que le schema d'url
        if (mb_strpos($url, '//') === 0) {
            // extrait le schema depuis $feed->website
            $schema = parse_url($feed->website, PHP_URL_SCHEME);

            return $schema.':'.$url;
        }

        return (string) SimplePie_IRI::absolutize($feed->website, $url);
    }
}
