<?php

namespace Clearss\Http\Controllers;

use Clearss\Models\Entry;
use Clearss\Services\OplmService;
use GuzzleHttp\Client;
use GuzzleHttp\Pool;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use Log;

class TestController extends Controller
{

    /**
     * @var \Clearss\Services\OplmService
     */
    protected $opml;

    public function __construct(OplmService $opml)
    {
        $this->opml = $opml;
    }

    public function index()
    {
        $all = Entry::all();
        $entry = Entry::find(1);
        $entry->read = ! $entry->read;
        $entry->save();
    }

    public function index2()
    {
        $uris = [
            'http://fr.capstable.net/feed/podcast/',
        ];

        $client = new Client([
            'http_errors'     => false,
            'connect_timeout' => 0.50,
            'timeout'         => 10.00,
            'headers'         => [
                'User-Agent' => 'Test/1.0',
            ],
        ]);

        $requests = function () use ($uris) {
            foreach ($uris as $uri) {
                yield new Request('GET', $uri);
            }
            /*            for ($i = 0; $i < count($uris); $i++) {
                            yield new Request('GET', $uris[$i]);
                        }*/
        };

        $pool = new Pool($client, $requests(), [
            'concurrency' => 10,
            'fulfilled'   => function (Response $response, int $index) {
                dump($response->getBody()->getContents());
                // this is delivered each successful response
                print_r($index."fulfilled\n");
                Log::info($index.'fulfilled');
                // save entry to db
            }, /*,
            'rejected'    => function ($reason, $index) {
                // this is delivered each failed request
                print_r($index."rejected\n");
                \Log::info($index."rejected".$reason);
            },*/
        ]);

        // Initiate the transfers and create a promise
        $promise = $pool->promise();
        // Force the pool of requests to complete.
        $promise->wait();
    }
}
