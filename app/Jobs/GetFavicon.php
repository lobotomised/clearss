<?php

namespace Clearss\Jobs;

use Clearss\Models\Feed;
use Clearss\Support\Favicon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class GetFavicon implements ShouldQueue
{

    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * The number of times the job may be attempted.
     *
     * @var int
     */
    public $tries = 5;

    /**
     * @var \Clearss\Models\Feed
     */
    protected $feed;

    /**
     * Create a new job instance.
     *
     * @param  \Clearss\Models\Feed  $feed
     */
    public function __construct(Feed $feed)
    {
        $this->feed = $feed;
    }

    /**
     * Execute the job.
     *
     * @param  \Clearss\Support\Favicon  $favicon
     *
     * @return void
     */
    public function handle(Favicon $favicon)
    {
        $favicon->refresh($this->feed);
    }
}
