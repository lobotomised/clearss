<?php

namespace Clearss\Services;

use Carbon\Carbon;
use Clearss\Jobs\FetchFeed;
use Clearss\Models\Entry;
use Clearss\Models\Feed;
use Clearss\Repositories\EntryRepository;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use SimplePie;

class Updater
{

    /**
     * @var \Clearss\Repositories\EntryRepository
     */
    private $entryRepository;

    public function __construct(EntryRepository $entryRepository)
    {
        $this->entryRepository = $entryRepository;
    }

    public function fetchAllFeed(): void
    {
        Feed::all()->sortBy('last_batch')->each(function ($feed) {
            $this->fetchFeed($feed);
        });
    }

    public function fetchFeed(Model $feed): void
    {
        $ttl = $feed->ttl ?: config('rss.day_retention');
        if ($feed->last_batch && $feed->last_batch->addMinutes($ttl)->gt(now())) {
            return;
        }

        $cache = config('rss.cache_path');

        $cache_path = storage_path('app/'.$cache);
        if ( ! is_dir($cache_path)) {
            Storage::makeDirectory($cache);
        }

        $sp = new SimplePie();
        $sp->set_cache_duration($feed->ttl ?: 1800);
        $sp->set_cache_location($cache_path);
        $sp->set_feed_url($feed->url);
        $sp->init();

        $sp->handle_content_type();

        $new_entries = 0;
        foreach ($sp->get_items() as $item) {
            $pub_date = Carbon::createFromTimestamp($item->get_date('U'));

            // Item is to old
            if (Carbon::now()->subDays(config('rss.day_retention'))->gt($pub_date)) {
                continue;
            }

            // Already exist
            if (Entry::whereGuid($item->get_permalink())->count() > 0) {
                continue;
            }

            $entry = $this->entryRepository->createEntry(
                $item->get_title(),
                $item->get_content() ?: '',
                $item->get_link(0),
                $item->get_permalink(),
                $feed->id
            );

            if ($entry->exists && $entry->wasRecentlyCreated) {
                $new_entries++;
            }
        }

        // touch the feed.last_batch
        Feed::whereId($feed->id)->update(['last_batch' => now()]);

        if ($new_entries > 0) {
            Feed::whereId($feed->id)->increment('nb_entries', $new_entries);
            Feed::whereId($feed->id)->increment('nb_entries_unread', $new_entries);
        }
    }
}
