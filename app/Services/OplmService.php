<?php

namespace Clearss\Services;

use Clearss\Exceptions\FileOpmlNotFoundException;
use Clearss\Models\Category;
use Clearss\Models\Feed;
use imelgrat\OPML_Parser\OPML_Parser;

class OplmService
{

    public function export()
    {
        // TODO Implement
    }

    /**
     * @param $file
     *
     * @throws \Clearss\Exceptions\FileOpmlNotFoundException
     */
    public function import($file)
    {
        if ( ! file_exists($file)) {
            throw new FileOpmlNotFoundException;
        }

        $parser = new OPML_Parser();

        $parser->ParseLocation($file);

        $current_category = null;
        foreach ($parser as $item) {
            if ( ! isset($item['TYPE'])) {
                $category = new Category;

                $category->name = $item['TEXT'];
                $category->user_id = 1; // TODO Auth

                $category->save();

                $current_category = $category;
            } else {
                if ($current_category === null) {
                    continue;
                }

                $feed = new Feed;

                $feed->url = $item['XMLURL'];
                $feed->name = $item['TEXT'];
                $feed->website = $item['HTMLURL'];
                $feed->user_id = 1; // TODO Auth
                $feed->category_id = $current_category->id;

                $feed->save();
            }
        }
    }
}
