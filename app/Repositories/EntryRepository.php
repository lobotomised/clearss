<?php

namespace Clearss\Repositories;

use Clearss\Models\Entry;

class EntryRepository
{

    /**
     * @var \Clearss\Models\Entry
     */
    private $entry;

    public function __construct(Entry $entry)
    {
        $this->entry = $entry;
    }

    /**
     * @param  string  $title
     * @param  string  $content
     * @param  string  $url
     * @param  string  $permalink
     * @param  int  $feed_id
     *
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function createEntry(
        string $title,
        string $content,
        string $url,
        string $permalink,
        int $feed_id
    ) {
        return $this->entry->newQuery()->create([
            'title' => $title,
            'content' => $content,
            'url' => $url,
            'guid' => $permalink,
            'feed_id' => $feed_id,
        ]);
    }
}
