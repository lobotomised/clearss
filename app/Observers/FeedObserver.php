<?php

namespace Clearss\Observers;

use Clearss\Jobs\GetFavicon;
use Clearss\Models\Feed;
use Clearss\Support\Favicon;

class FeedObserver
{

    /**
     * Handle the feed "created" event.
     *
     * @param  \Clearss\Models\Feed  $feed
     *
     * @return void
     */
    public function created(Feed $feed)
    {
        GetFavicon::dispatch($feed);
    }

    /**
     * Handle the feed "updated" event.
     *
     * @param  \Clearss\Models\Feed  $feed
     *
     * @return void
     * @throws \Exception
     */
    public function updated(Feed $feed)
    {
        // 1% chance to update favicon
        if (random_int(1, 100) === 1) {
            GetFavicon::dispatch($feed);
        }
    }

    /**
     * Handle the feed "deleted" event.
     *
     * @param  \Clearss\Models\Feed  $feed
     *
     * @return void
     */
    public function deleted(Feed $feed)
    {
        $favicon = new Favicon();
        $favicon->deleteFavicon($feed);
    }
}
