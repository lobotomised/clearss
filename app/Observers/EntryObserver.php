<?php

namespace Clearss\Observers;

use Clearss\Models\Entry;

class EntryObserver
{

    /**
     * Handle the entry "created" event.
     *
     * @param  \Clearss\Models\Entry  $entry
     *
     * @return void
     */
    public function created(Entry $entry)
    {
        //
    }

    /**
     * Handle the entry "updating" event.
     *
     * @param  \Clearss\Models\Entry  $entry
     */
    public function updating(Entry $entry)
    {

        // Si on tag l'entry comme étant lu, on va mettre à jour le feed.nb_entries_unread --
        if ($entry->read && $entry->getOriginal('read') === 0) {
            $entry->feed()->decrement('nb_entries_unread', 1);
        }
        // Si on tag l'entry comme étant non-lu, on va mettre à jour le feed.nb_entries_unread ++
        if ( ! $entry->read && $entry->getOriginal('read') === 1) {
            $entry->feed()->increment('nb_entries_unread', 1);
        }
    }

    /**
     * Handle the entry "updated" event.
     *
     * @param  \Clearss\Models\Entry  $entry
     *
     * @return void
     */
    public function updated(Entry $entry)
    {
        //
    }

    /**
     * Handle the entry "deleted" event.
     *
     * @param  \Clearss\Models\Entry  $entry
     *
     * @return void
     */
    public function deleted(Entry $entry)
    {
        //
    }

    /**
     * Handle the entry "restored" event.
     *
     * @param  \Clearss\Models\Entry  $entry
     *
     * @return void
     */
    public function restored(Entry $entry)
    {
        //
    }

    /**
     * Handle the entry "force deleted" event.
     *
     * @param  \Clearss\Models\Entry  $entry
     *
     * @return void
     */
    public function forceDeleted(Entry $entry)
    {
        //
    }
}
