<?php

namespace Clearss\Models;

use Clearss\Observers\EntryObserver;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * Clearss\Models\Entry.
 *
 * @property int $id
 * @property int $feed_id
 * @property string $title
 * @property string|null $content
 * @property string $url
 * @property string $guid
 * @property bool $read
 * @property bool $favorite
 * @property \Illuminate\Support\Carbon $last_updated_at
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Clearss\Models\Feed $feed
 * @method static Builder|Entry favorite()
 * @method static Builder|Entry newModelQuery()
 * @method static Builder|Entry newQuery()
 * @method static Builder|Entry query()
 * @method static Builder|Entry unread()
 * @method static Builder|Entry whereContent($value)
 * @method static Builder|Entry whereCreatedAt($value)
 * @method static Builder|Entry whereFavorite($value)
 * @method static Builder|Entry whereFeedId($value)
 * @method static Builder|Entry whereGuid($value)
 * @method static Builder|Entry whereId($value)
 * @method static Builder|Entry whereLastUpdatedAt($value)
 * @method static Builder|Entry whereRead($value)
 * @method static Builder|Entry whereTitle($value)
 * @method static Builder|Entry whereUpdatedAt($value)
 * @method static Builder|Entry whereUrl($value)
 * @mixin \Eloquent
 */
class Entry extends Model
{

    protected $fillable = [
        'title', 'content', 'url', 'read', 'favorite', 'guid', 'last_updated_at',
        'feed_id',
    ];

    protected $casts = [
        'content'         => 'string',
        'read'            => 'boolean',
        'favorite'        => 'boolean',
        'last_updated_at' => 'datetime',
    ];

    public static function boot()
    {
        parent::boot();

        self::observe(new EntryObserver);

        static::addGlobalScope('OrderByUpdate', static function (Builder $builder) {
            $builder->orderByDesc('last_updated_at');
        });
    }

    public function feed()
    {
        return $this->belongsTo(Feed::class);
    }

    public function scopeByRead(Builder $query)
    {
        $query->where('unread', '=', 1);
    }

    public function scopeByUnread(Builder $query)
    {
        $query->where('unread', '=', 1);
    }

    public function scopeFavorite(Builder $query)
    {
        $query->where('favorite', '=', 1);
    }

    public function setTitleAttribute($value): void
    {
        $this->attributes['title'] = mb_substr($value, 0, 255);
    }
}
