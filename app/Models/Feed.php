<?php

namespace Clearss\Models;

use Clearss\Observers\FeedObserver;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

/**
 * Clearss\Models\Feed.
 *
 * @property int $id
 * @property int $user_id
 * @property int $category_id
 * @property string $name
 * @property string $url
 * @property string $website
 * @property \Illuminate\Support\Carbon|null $last_batch
 * @property bool $error
 * @property int|null $ttl
 * @property int $nb_entries
 * @property int $nb_entries_unread
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Clearss\Models\Category $category
 * @property-read \Illuminate\Database\Eloquent\Collection|\Clearss\Models\Entry[] $entries
 * @property-read \Clearss\Models\User $user
 * @method static Builder|Feed newModelQuery()
 * @method static Builder|Feed newQuery()
 * @method static Builder|Feed query()
 * @method static Builder|Feed whereCategoryId($value)
 * @method static Builder|Feed whereCreatedAt($value)
 * @method static Builder|Feed whereError($value)
 * @method static Builder|Feed whereId($value)
 * @method static Builder|Feed whereLastBatch($value)
 * @method static Builder|Feed whereName($value)
 * @method static Builder|Feed whereNbEntries($value)
 * @method static Builder|Feed whereNbEntriesUnread($value)
 * @method static Builder|Feed whereTtl($value)
 * @method static Builder|Feed whereUpdatedAt($value)
 * @method static Builder|Feed whereUrl($value)
 * @method static Builder|Feed whereUserId($value)
 * @method static Builder|Feed whereWebsite($value)
 * @mixin \Eloquent
 */
class Feed extends Model
{

    protected $fillable = [
        'name', 'url', 'website', 'error', 'ttl', 'nb_entries', 'nb_entries_unread',
    ];

    protected $casts = [
        'last_batch' => 'datetime',
        'error'      => 'boolean',
    ];

    public static function boot()
    {
        parent::boot();

        self::observe(new FeedObserver);

        // seulement les feed de l'utilisateur courant
        static::addGlobalScope('ForUser', static function (Builder $query) {
            $query->where('user_id', '=', 1); // TODO: remplacer par Auth::User()->id
        });

        // ordonne les flux par non
        static::addGlobalScope('OrderByName', static function (Builder $query) {
            $query->orderBy('name', 'ASC');
        });
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function entries()
    {
        return $this->hasMany(Entry::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
